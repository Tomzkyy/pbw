<html lang='en'>

<head>
    <title>Formulir</title>
</head>

<body>
    <h2>Formulir Pendaftaran Ketua Osis</h2>
    <form action='proses.php' method='post'>
        <label>Nama Lengkap:</label>
        <input type='text' name='nama' placeholder="Masukkan nama lengkap" autocomplete="off"><br>
        <label>Alamat:</label>
        <textarea name='alamat' placeholder="Masukkan alamat lengkap" required></textarea><br>
        <label>jenis kelamin:</label>
        <input type='radio' name='jk' value='Laki-laki'>Laki-laki
        <input type='radio' name='jk' value='Perempuan'>Perempuan<br>
        <label>hobi:</label>
        <input type='checkbox' name='hobi[]' value='Membaca'>Membaca
        <input type='checkbox' name='hobi[]' value='Menulis'>Menulis
        <input type='checkbox' name='hobi[]' value='Menggambar'>Menggambar<br>
        <label>Tanggal Lahir:</label>
        <input type='date' name='tgl' min="2000-01-01" max="2000-12-31" required><br>
        <label>Warna Kesukaan:</label>
        <input type='color' name='warna'><br>
        <label>Pekerjaan:</label>
        <select name='pekerjaan'>
            <option value=''>--Pilih Pekerjaan--</option>
            <option value='Pelajar'>Pelajar</option>
            <option value='Mahasiswa'>Mahasiswa</option>
            <option value='Rebahan'>Rebahan</option>
        </select><br>
        <label>Jurusan:</label>
        <input type="text" list="jurusans" name="jurusan" required>
        <datalist id="jurusans">
            <option value="Teknik Informatika"> Teknik Informatika</option>
            <option value="Teknik Mesin"> Teknik Mesin</option>
            <option value="Teknik Industri"> Teknik Industri</option>
            <option value="Teknik Elektro"> Teknik Elektro</option>
            <option value="Teknik Sipil"> Teknik Sipil</option>
        </datalist>
        <br>
        <input type='submit' name='tbl' value='Simpan'>
        <input type='reset' value='Reset'>
    </form>
</body>

</html>